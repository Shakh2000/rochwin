# Rochwin



## Getting started
1. <a name="introduction">Introduction</a><br>
2. <a name="setting-up-the-development-environment">Setting Up the Development Environment </a><br>
<a name="requirements">Install requirements/base.txt </a><br>
<a name="code-style-with-flake8">Code Style with Flake8 </a><br>
<a name="running-tests-with-pytest">Running Tests with Pytest </a>
3. <a name="docker-setup">Docker Setup </a><br>
<a name="creating-envdev-file">Creating .env.dev File </a><br>
<a name="configuring-envdev">Configuring .env.dev </a>
4. <a name="docker-run">Run project </a><br>

# Introduction
    This project is for managing business related to medical devices selling.

# Setting Up the Development Environment
    - pip install -r requirements for local without docker if you want
    - flake8 your_project_directory
        for example: 
            flake8 src/
    - running tests with pytest
            run command pytest
            run command pytest --cov=src --cov-report html for coverage tests
# Docker Setup
    - creating .env.dev file according to .env.sample
    - configure .env.dev

# Run project 
    - run command docker compose up --build -d (-d allows the container to run in the background)