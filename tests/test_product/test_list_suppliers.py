import pytest
from rest_framework.test import APIClient
from src.shared.test import BaseTest


class TestListCategory(BaseTest):
    fixtures = [
        "tests/test_product/fixtures/suppliers.json"
    ]

    @pytest.mark.django_db
    def test_list_categories(self, client=APIClient()):
        response = client.get('/product/supplier/list/')
        assert response.status_code == 200
        body = response.json()
        assert body["results"][0]["id"] == 1
