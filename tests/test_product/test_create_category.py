import pytest
from rest_framework.test import APIClient
from src.shared.test import BaseTest


class TestListCategory(BaseTest):
    fixtures = [
        "tests/test_product/fixtures/categories.json"
    ]

    @pytest.mark.django_db
    def test_list_categories(self, client=APIClient()):
        data = dict(
            id=100,
            name="Medical Devices Test"
        )

        response = client.post(
            '/product/category/create/',
            data=data
        )

        assert response.status_code == 201
        body = response.json()
        assert body["id"] == 4
