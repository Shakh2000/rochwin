import pytest
from rest_framework.test import APIClient
from src.shared.test import BaseTest


class TestDetailProduct(BaseTest):
    fixtures = [
        "tests/test_product/fixtures/categories.json",
        "tests/test_product/fixtures/suppliers.json",
        "tests/test_product/fixtures/products.json"
    ]

    @pytest.mark.django_db
    def test_detail_product(self, client=APIClient()):
        response = client.get('/product/detail/3/')
        assert response.status_code == 200
        body = response.json()
        assert body["id"] == 3
        assert body["name"] == "Manufacturing Supplier"
        assert body["description"] == "Good for health"
        assert body["manufacturer"] == "AsiaMed"
        assert body["model_number"] == "4578"
        assert body["serial_number"] == "AFC65214"
        assert body["date_manufactured"] == "2023-11-29"
        assert body["date_purchased"] == "2023-12-01"
        assert body["dimensions"] == ""
        assert body["weight"] == "12.00"
        assert body["power_requirements"] == "12kwt"
        assert body["certification_number"] == "AC1254"
        assert body["regulatory_approval"] == ""
        assert body["price"] == "13.00"
        assert body["quantity_in_stock"] == 20

        category = body["category"]
        assert category["name"] == "Medical Devices"

        supplier = body["supplier"]
        assert supplier["id"] == 1
        assert supplier["name"] == "Manufacturing Supplier"
        assert supplier["contact_person"] == "Shakhzod"
        assert supplier["phone_number"] == "+998995558076"
