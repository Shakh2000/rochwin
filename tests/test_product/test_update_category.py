import pytest
from rest_framework.test import APIClient
from src.shared.test import BaseTest


class TestUpdateCategory(BaseTest):
    fixtures = [
        "tests/test_product/fixtures/categories.json"
    ]

    @pytest.mark.django_db
    def test_update_categories(self, client=APIClient()):
        data = dict(
            name="Medical Devices Test Updated"
        )

        response = client.patch(
            '/product/category/update/3/',
            data=data
        )

        assert response.status_code == 200
        body = response.json()
        assert body["id"] == 3
        assert body["name"] == "Medical Devices Test Updated"
