import pytest
from rest_framework.test import APIClient
from src.shared.test import BaseTest


class TestListCategory(BaseTest):
    fixtures = [
        "tests/test_product/fixtures/categories.json",
        "tests/test_product/fixtures/suppliers.json",
        "tests/test_product/fixtures/products.json"
    ]

    @pytest.mark.django_db
    def test_list_categories(self, client=APIClient()):
        response = client.get('/product/list/')
        assert response.status_code == 200
        body = response.json()
        assert body["results"][0]["id"] == 1
