import pytest
from rest_framework.test import APIClient
from src.shared.test import BaseTest


class TestListCustomer(BaseTest):
    fixtures = [
        "tests/test_customer/fixtures/customers.json"
    ]

    @pytest.mark.django_db
    def test_list_customers(self, client=APIClient()):
        response = self.client.get('/product/category/list/')
        assert response.status_code == 200
        body = response.json()
        assert body["results"][0]["id"] == 1
