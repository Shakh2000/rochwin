from django.test import Client
from functools import wraps


def api_client(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        with Client() as c:
            kwargs['client'] = c
            return func(*args, **kwargs)

    return wrapper
