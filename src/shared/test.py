import os
from django.test import TestCase
from django.core.management import call_command

from config import settings


class BaseTest(TestCase):
    fixtures = []
    fixture_path = os.path.join(settings.BASE_DIR, "tests")

    def load_fixture(self, fixture_name):
        fixture_file_path = os.path.join(self.fixture_path, f"{fixture_name}.json")
        call_command('loaddata', fixture_file_path)
