from django.db import models
from src.core.models.user import CustomUser


class Customer(CustomUser):
    name = models.CharField(max_length=255, blank=True, null=True)
    image = models.ImageField(upload_to='customer/', blank=True, null=True)
    dpi = models.CharField(max_length=255, blank=True, null=True)
    telegram = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return f"{self.id} --> {self.name if self.name else None}"

    def get_full_name(self):
        return f'{self.first_name and self.last_name}'

    class Meta:
        db_table = "customer"
