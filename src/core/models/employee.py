from django.db import models
from typing import TypedDict
from src.core.models.user import CustomUser


class GenderChoices(models.TextChoices):
    MALE = "male"
    FEMALE = "female"


class StatusChoices(models.TextChoices):
    ACTIVE = "active"
    ON_LEAVE = "on_leave"
    ON_VACATION = "on_vacation"
    DISMISSED = "dismissed"


class MaritalStatusChoices(models.TextChoices):
    SINGLE = "single"
    MARRIED = "married"
    DIVORCED = "divorced"
    WIDOWED = "widowed"


class EmergencyContactChoices(models.TextChoices):
    BROTHER = "brother"
    SISTER = "sister"
    FATHER = "father"
    MOTHER = "mother"
    SPOUSE = "spouse"
    PARTNER = "partner"
    FRIEND = "friend"
    COLLEAGUE = "colleague"
    NEIGHBOR = "neighbor"
    OTHER = "other"


class EmployeeShiftChoices(models.TextChoices):
    DAY = "day"
    NIGHT = "night"
    HYBRID = "hybrid"


class PayrollPolicyChoices(models.TextChoices):
    MONTHLY = "monthly"
    TWICE_A_MONTH = "twice_a_month"
    FORTNIGHT = "fortnight"
    WEEKLY = "weekly"


class Address(TypedDict):
    street: str
    city: str
    state: str
    zipcode: str


class EmergencyContact(TypedDict):
    first_name: str
    last_name: str
    phone_number: str
    relationship: str


class ProfessionAndAcademics(TypedDict):
    job_position: str
    profession: str
    academic_level: str
    occupation: str
    sub_occupation: str


class Employee(CustomUser):
    # Employee shift type
    DAY = "day"
    NIGHT = "night"
    HYBRID = "hybrid"

    status = models.CharField(max_length=50, choices=StatusChoices.choices, default=StatusChoices.ACTIVE)
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=50, choices=GenderChoices.choices, blank=True, null=True)
    dpi = models.CharField(max_length=255, blank=True, null=True)
    nit = models.CharField(max_length=255, blank=True, null=True)
    shift_type = models.CharField(max_length=50, choices=EmployeeShiftChoices.choices, blank=True, null=True)
    nationality = models.CharField(max_length=50, blank=True, null=True)
    marital_status = models.CharField(max_length=50, choices=EmployeeShiftChoices.choices, blank=True, null=True)
    profession_and_academics: ProfessionAndAcademics = models.JSONField(default=dict, blank=True, null=True)
    address: Address = models.JSONField(default=dict, blank=True, null=True)
    emergency_contact: EmergencyContact = models.JSONField(default=dict, blank=True, null=True)
    hired_date = models.DateField(blank=True, null=True)
    payroll_policy = models.CharField(max_length=50, choices=PayrollPolicyChoices.choices, blank=True, null=True)
    vacation_balance = models.FloatField(default=0)

    @property
    def daily_working_hours(self) -> int:
        _map = {
            Employee.DAY: 8,
            Employee.HYBRID: 7,
            Employee.NIGHT: 6,
        }
        return _map[self.shift_type]

    class Meta:
        db_table = "employee"
