from django.db import models


class Product(models.Model):
    # General Information
    name = models.CharField(max_length=255, verbose_name='Product Name')
    description = models.TextField(blank=True, null=True)
    manufacturer = models.CharField(max_length=255)
    model_number = models.CharField(max_length=50, blank=True, null=True)
    serial_number = models.CharField(max_length=50, blank=True, null=True)
    date_manufactured = models.DateField(blank=True, null=True)
    date_purchased = models.DateField(blank=True, null=True)

    # Technical Specifications
    dimensions = models.CharField(max_length=100, blank=True, null=True)
    weight = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    power_requirements = models.CharField(max_length=100, blank=True, null=True)

    # Compliance and Certification
    certification_number = models.CharField(max_length=50, blank=True, null=True)
    regulatory_approval = models.CharField(max_length=100, blank=True, null=True)

    # Pricing and Inventory
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    quantity_in_stock = models.PositiveIntegerField(default=0)

    # Relationships
    category = models.ForeignKey('Category', on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name='Product Category', related_name="category")
    supplier = models.ForeignKey('Supplier', on_delete=models.SET_NULL, null=True, blank=True, related_name="supplier")

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Supplier(models.Model):
    name = models.CharField(max_length=255)
    contact_person = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return self.name
