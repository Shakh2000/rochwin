from django.db import models
from src.core.models.customer import Customer
from src.core.models.employee import Employee
from src.core.models.product import Product


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name="customer")
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="employee")
    products = models.ManyToManyField(Product, through='OrderItem')
    order_date = models.DateField(auto_now_add=True)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def calculate_total_amount(self):
        self.total_amount = sum(item.total_price() for item in self.order_items.all())
        self.save()

    def __str__(self):
        return f"Order #{self.id} - {self.customer.name}"


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    def total_price(self):
        return self.product.price * self.quantity

    def __str__(self):
        return f"{self.quantity} x {self.product.name} in Order #{self.order.id}"
