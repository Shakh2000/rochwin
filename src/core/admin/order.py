from src.core.models import order
from django.contrib import admin


@admin.register(order.Order)
class OrderModelAdmin(admin.ModelAdmin):
    search_fields = ("order_date",)
    list_display = ("id", "order_date")
    list_display_links = ("id", "order_date")


@admin.register(order.OrderItem)
class OrderItemModelAdmin(admin.ModelAdmin):
    search_fields = ("order", "quantity")
    list_display = ("id", "order")
    list_display_links = ("id", "order")
