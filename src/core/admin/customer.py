from src.core.models import customer
from django.contrib import admin


@admin.register(customer.Customer)
class CustomerModelAdmin(admin.ModelAdmin):
    search_fields = ("first_name",)
    list_display = ("id", "first_name")
    list_display_links = ("id", "first_name")
