from src.core.models import employee
from django.contrib import admin


@admin.register(employee.Employee)
class EmployeeModelAdmin(admin.ModelAdmin):
    search_fields = ("first_name",)
    list_display = ("id", "first_name")
    list_display_links = ("id", "first_name")
