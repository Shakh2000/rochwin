from src.core.models import product
from django.contrib import admin


@admin.register(product.Supplier)
class SupplierModelAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("id", "name")
    list_display_links = ("id", "name")


@admin.register(product.Category)
class CategoryModelAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("id", "name")
    list_display_links = ("id", "name")


@admin.register(product.Product)
class ProductModelAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("id", "name")
    list_display_links = ("id", "name")
