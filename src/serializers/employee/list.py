from rest_framework import serializers
from src.core.models import employee


class ListEmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = employee.Employee
        fields = ("id", "first_name", "last_name", "email", "phone_number")
