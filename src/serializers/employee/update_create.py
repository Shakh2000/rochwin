from rest_framework import serializers
from src.core.models import employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = employee.Employee
        fields = "__all__"


class StatisticEmployeeSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    number_of_customers = serializers.IntegerField()
    number_of_products = serializers.IntegerField()
    number_of_products_sold_out = serializers.IntegerField()
