from rest_framework import serializers
from src.core.models import product
from src.serializers.product.category import ProductCategorySerializer
from src.serializers.product.supplier import ProductSupplierSerializer


class DetailProductSerializer(serializers.ModelSerializer):
    category = ProductCategorySerializer()
    supplier = ProductSupplierSerializer()

    class Meta:
        model = product.Product
        fields = "__all__"
        depth = 1
