from rest_framework import serializers
from src.core.models import product


class ListProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = product.Product
        fields = ("id", "name", "price", "weight", "quantity_in_stock")
        depth = 1
