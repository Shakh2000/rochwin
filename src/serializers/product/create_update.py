from rest_framework import serializers
from src.core.models import product


class CreateUpdateProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = product.Product
        fields = "__all__"
