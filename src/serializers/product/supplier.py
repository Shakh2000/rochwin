from rest_framework import serializers
from src.core.models import product


class ProductSupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = product.Supplier
        fields = "__all__"
