from rest_framework import serializers
from src.core.models import product


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = product.Category
        fields = ("name",)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = product.Category
        fields = ("id", "name")
