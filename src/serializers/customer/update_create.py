from rest_framework import serializers
from src.core.models import customer


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = customer.Customer
        fields = ("id", "first_name", "last_name", "email", "phone_number")
