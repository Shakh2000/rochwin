from rest_framework import serializers
from src.core.models import order


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = order.OrderItem
        fields = "__all__"


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = order.Order
        fields = "__all__"
