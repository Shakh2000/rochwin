from rest_framework import serializers
from src.core.models import order
from src.serializers.customer.list import ListCustomerSerializer
from src.serializers.product.list import ListProductSerializer


class ListOrderItemSerializer(serializers.ModelSerializer):
    product = ListProductSerializer()

    class Meta:
        model = order.OrderItem
        exclude = ("order",)
        depth = 1


class ListOrderSerializer(serializers.ModelSerializer):
    customer = ListCustomerSerializer()
    products = ListProductSerializer()

    class Meta:
        model = order.Order
        fields = "__all__"
        depth = 1
