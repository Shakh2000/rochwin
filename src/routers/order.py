from django.urls import path
from src.services.order import list, detail, create, update, delete
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('list', list.ListOrderView)
router.register('detail', detail.DetailOrderView)
router.register('create/item', create.CreateOrderItemView)
router.register('create', create.CreateOrderView)
router.register('update', update.UpdateOrderView)
router.register('update/item', update.UpdateOrderItemView)
router.register('delete', delete.DeleteOrderView)
router.register('delete/item', delete.DeleteOrderItemView)

urlpatterns = [
    path('<int:pk>/items/list', list.ListOrderItemView.as_view(), name='order-items-by-order-id')
] + router.urls
