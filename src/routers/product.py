from src.services.product import list, detail, create, update, delete
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('list', list.ListProductView)
router.register('detail', detail.DetailProductView)
router.register('create', create.CreateProductView)
router.register('update', update.UpdateProductView)
router.register('delete', delete.DeleteProductView)
router.register('category/list', list.ListCategoryView)
router.register('category/detail', detail.DetailCategoryView)
router.register('category/create', create.CreateCategoryView)
router.register('category/update', update.UpdateCategoryView)
router.register('category/delete', delete.DeleteCategoryView)
router.register('supplier/list', list.ListSupplierView)
router.register('supplier/detail', detail.DetailSupplierView)
router.register('supplier/create', create.CreateSupplierView)
router.register('supplier/update', update.UpdateSupplierView)
router.register('supplier/delete', delete.DeleteSupplierView)

urlpatterns = [

] + router.urls
