from django.urls import path
from src.services.employee import list, detail, create, update, delete
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('list', list.ListEmployeeView)
router.register('detail', detail.DetailEmployeeView)
router.register('create', create.CreateEmployeeView)
router.register('update', update.UpdateEmployeeView)
router.register('delete', delete.DeleteEmployeeView)

urlpatterns = [
    path('statistics/employee/<int:id>/month', detail.StatisticDetailEmployeeView.as_view())
] + router.urls
