from django.urls import path

from src.services.customer import list, detail, create, update, delete
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('list', list.ListCustomerView)
router.register('detail', detail.DetailCustomerView)
router.register('create', create.CreateCustomerView)
router.register('update', update.UpdateCustomerView)
router.register('delete', delete.DeleteCustomerView)

urlpatterns = [
    path('statistics/client/<int:id>/', detail.StatisticDetailCustomerView.as_view())
] + router.urls
