from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import customer
from src import serializers
from config.pagination import MyPagination


class ListCustomerView(ListModelMixin, GenericViewSet):
    queryset = customer.Customer.objects.all().order_by("id")
    serializer_class = serializers.ListCustomerSerializer
    pagination_class = MyPagination
