from rest_framework.mixins import DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import customer


class DeleteCustomerView(DestroyModelMixin, GenericViewSet):
    queryset = customer.Customer.objects.all()
