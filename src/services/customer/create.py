from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import customer
from src import serializers


class CreateCustomerView(CreateModelMixin, GenericViewSet):
    queryset = customer.Customer.objects.all()
    serializer_class = serializers.CustomerSerializer
