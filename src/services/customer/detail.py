from django.shortcuts import get_object_or_404
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.views import APIView, Response, status
from rest_framework.viewsets import GenericViewSet
from src.core.models import customer as models
from src import serializers

from django.db.models import Count, IntegerField, Sum
from django.db.models.functions import Coalesce


class DetailCustomerView(RetrieveModelMixin, GenericViewSet):
    queryset = models.Customer.objects.all()
    serializer_class = serializers.CustomerSerializer


class StatisticDetailCustomerView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}
        id = kwargs.get("id", None)
        if id:
            customer = get_object_or_404(models.Customer, id=id)

            data["full_name"] = customer.last_name
            statistic = customer.customer.aggregate(
                bought_product=Count("products", distinct=True),
                bought_product_quantity=Coalesce(Sum("order_items__quantity"),
                                                 0,
                                                 output_field=IntegerField()
                                                 ),
                bought_product_cost=Coalesce(Sum("total_amount"),
                                             0,
                                             output_field=IntegerField()
                                             ),
            )
            data["full_name"] = customer.get_full_name()
            data["identification"] = customer.dpi
            data["bought_product"] = statistic.get("bought_product", None)
            data["bought_product_quantity"] = statistic.get("bought_product_quantity", None)
            data["bought_product_cost"] = statistic.get("bought_product_cost", None)

        return Response(data=data, status=status.HTTP_200_OK)
