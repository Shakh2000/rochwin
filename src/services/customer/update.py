from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import customer
from src import serializers


class UpdateCustomerView(UpdateModelMixin, GenericViewSet):
    queryset = customer.Customer.objects.all()
    serializer_class = serializers.CustomerSerializer
