from rest_framework.mixins import RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import product
from src import serializers


class DetailProductView(RetrieveModelMixin, GenericViewSet):
    queryset = product.Product.objects.select_related('category', 'supplier')
    serializer_class = serializers.DetailProductSerializer


class DetailCategoryView(RetrieveModelMixin, GenericViewSet):
    queryset = product.Product.objects.all()
    serializer_class = serializers.CategorySerializer


class DetailSupplierView(RetrieveModelMixin, GenericViewSet):
    queryset = product.Product.objects.all()
    serializer_class = serializers.ProductSupplierSerializer
