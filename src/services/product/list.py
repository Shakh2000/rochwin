from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import product
from src import serializers
from config.pagination import MyPagination


class ListProductView(ListModelMixin, GenericViewSet):
    queryset = product.Product.objects.select_related('category', 'supplier').order_by("id")
    serializer_class = serializers.ListProductSerializer
    pagination_class = MyPagination


class ListCategoryView(ListModelMixin, GenericViewSet):
    queryset = product.Category.objects.all().order_by("id")
    serializer_class = serializers.CategorySerializer
    pagination_class = MyPagination


class ListSupplierView(ListModelMixin, GenericViewSet):
    queryset = product.Supplier.objects.all().order_by("id")
    serializer_class = serializers.ProductSupplierSerializer
    pagination_class = MyPagination
