from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import product
from src import serializers


class UpdateProductView(UpdateModelMixin, GenericViewSet):
    queryset = product.Product.objects.select_related('category', 'supplier')
    serializer_class = serializers.CreateUpdateProductSerializer


class UpdateCategoryView(UpdateModelMixin, GenericViewSet):
    queryset = product.Category.objects.all()
    serializer_class = serializers.CategorySerializer


class UpdateSupplierView(UpdateModelMixin, GenericViewSet):
    queryset = product.Category.objects.all()
    serializer_class = serializers.ProductSupplierSerializer
