from rest_framework.mixins import DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import product


class DeleteProductView(DestroyModelMixin, GenericViewSet):
    queryset = product.Product.objects.select_related('category', 'supplier')


class DeleteCategoryView(DestroyModelMixin, GenericViewSet):
    queryset = product.Product.objects.all()


class DeleteSupplierView(DestroyModelMixin, GenericViewSet):
    queryset = product.Product.objects.all()
