from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import product
from src import serializers


class CreateProductView(CreateModelMixin, GenericViewSet):
    queryset = product.Product.objects.select_related('category', 'supplier')
    serializer_class = serializers.CreateUpdateProductSerializer


class CreateCategoryView(CreateModelMixin, GenericViewSet):
    queryset = product.Category.objects.all()
    serializer_class = serializers.CategorySerializer


class CreateSupplierView(CreateModelMixin, GenericViewSet):
    queryset = product.Category.objects.all()
    serializer_class = serializers.ProductSupplierSerializer
