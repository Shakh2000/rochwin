from rest_framework.mixins import ListModelMixin
from rest_framework.views import APIView, Response, status
from rest_framework.viewsets import GenericViewSet
from src.core.models import order
from src import serializers
from config.pagination import MyPagination


class ListOrderItemView(APIView):
    pagination_class = MyPagination

    def get(self, request, *args, **kwargs):
        order_id = kwargs.get("pk")
        order_items = order.OrderItem.objects.filter(order_id=order_id).select_related("product").order_by("id")
        data = serializers.ListOrderItemSerializer(instance=order_items, many=True).data
        return Response(data=data, status=status.HTTP_200_OK)


class ListOrderView(ListModelMixin, GenericViewSet):
    queryset = order.Order.objects.select_related("customer").prefetch_related("products").order_by("id")
    serializer_class = serializers.ListOrderSerializer
    pagination_class = MyPagination
