from rest_framework.mixins import RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import order
from src import serializers


class DetailOrderView(RetrieveModelMixin, GenericViewSet):
    queryset = order.Order.objects.all()
    serializer_class = serializers.DetailOrderSerializer
