from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import order
from src import serializers


class CreateOrderItemView(CreateModelMixin, GenericViewSet):
    queryset = order.Order.objects.select_related("customer").prefetch_related("products")
    serializer_class = serializers.OrderItemSerializer


class CreateOrderView(CreateModelMixin, GenericViewSet):
    queryset = order.Order.objects.select_related("customer").prefetch_related("products")
    serializer_class = serializers.OrderSerializer
