from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import order
from src import serializers


class UpdateOrderView(UpdateModelMixin, GenericViewSet):
    queryset = order.Order.objects.all()
    serializer_class = serializers.OrderSerializer


class UpdateOrderItemView(UpdateModelMixin, GenericViewSet):
    queryset = order.Order.objects.all()
    serializer_class = serializers.OrderItemSerializer
