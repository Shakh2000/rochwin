from rest_framework.mixins import DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import order


class DeleteOrderView(DestroyModelMixin, GenericViewSet):
    queryset = order.Order.objects.all()


class DeleteOrderItemView(DestroyModelMixin, GenericViewSet):
    queryset = order.OrderItem.objects.all()
