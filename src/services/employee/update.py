from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import employee
from src import serializers


class UpdateEmployeeView(UpdateModelMixin, GenericViewSet):
    queryset = employee.Employee.objects.all()
    serializer_class = serializers.EmployeeSerializer
