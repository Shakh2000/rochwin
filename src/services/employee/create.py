from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import employee
from src import serializers


class CreateEmployeeView(CreateModelMixin, GenericViewSet):
    queryset = employee.Employee.objects.all()
    serializer_class = serializers.EmployeeSerializer
