from rest_framework.mixins import DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import employee


class DeleteEmployeeView(DestroyModelMixin, GenericViewSet):
    queryset = employee.Employee.objects.all()
