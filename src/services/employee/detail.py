from rest_framework.mixins import RetrieveModelMixin
from rest_framework.views import APIView, Response, status
from rest_framework.viewsets import GenericViewSet
from django.shortcuts import get_object_or_404
from src.core.models import employee as models
from src import serializers

from django.db.models import Count, IntegerField, Sum
from django.db.models.functions import Coalesce


class DetailEmployeeView(RetrieveModelMixin, GenericViewSet):
    queryset = models.Employee.objects.all()
    serializer_class = serializers.EmployeeSerializer


class StatisticDetailEmployeeView(APIView):
    def get(self, request, *args, **kwargs):
        data = {}
        id = kwargs.get("id", None)
        if id:
            employee = get_object_or_404(models.Employee, id=id)

            data["full_name"] = employee.last_name
            statistic = employee.employee.aggregate(
                customer=Coalesce(Count("customer"),
                                  0,
                                  output_field=IntegerField()
                                  ),
                sold_product=Count("products"),
                sold_product_quantity=Coalesce(Sum("order_items__quantity"),
                                               0,
                                               output_field=IntegerField()
                                               )
            )
            customer = employee.employee.values_list("customer").distinct().count()
            data["sold_product"] = statistic.get("sold_product", None)
            data["sold_product_quantity"] = statistic.get("sold_product_quantity", None)
            data["customer"] = customer

        return Response(data=data, status=status.HTTP_200_OK)
