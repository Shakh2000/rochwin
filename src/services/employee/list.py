from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from src.core.models import employee
from src import serializers
from config.pagination import MyPagination


class ListEmployeeView(ListModelMixin, GenericViewSet):
    queryset = employee.Employee.objects.all().order_by("id")
    serializer_class = serializers.ListEmployeeSerializer
    pagination_class = MyPagination
