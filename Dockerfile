FROM python:3.10

WORKDIR /usr/src/app

ENV PYTHONDONTWRITENYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
RUN pip install pytest
RUN pip install pytest-django

COPY requirements/base.txt /usr/src/app
COPY requirements/dev.txt /usr/src/app

RUN python -m pip install daphne
RUN python -m pip install --upgrade pip
RUN pip install -r dev.txt

COPY . /usr/src/app
